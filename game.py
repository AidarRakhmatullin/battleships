import time

start_time = time.time()
import openpyxl
from Classes.Battle import Battle
from Classes.AI import AI_shot
from Classes.strings import INTRO, GAME_START
s = 0
game = Battle()

battle_grownd_AI = [[s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10]
AI_ships = []
game.fild_creater(battle_grownd_AI, AI_ships, "AI")

battle_grownd_player = [[s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10, [s] * 10,
                        [s] * 10]
player_ships = []
print(INTRO)
print("Выберите способ создвния поля:\nm - вручную, a - автоматически")
while True:
    way = input()
    if way == "m":
        game.fild_creater(battle_grownd_player, player_ships, "gamer")
        break
    if way == "a":
        game.fild_creater(battle_grownd_player, player_ships, "AI")
        break
    if way == "d":
        game.fild_creater(battle_grownd_player, player_ships, "AI")
        break
    print("выберите коректную опцию")

start_time = time.time()
print("Ваше поле")
print("----------------------------------------")
game.printer(battle_grownd_player, True)
print("----------------------------------------")

player = "gamer"  # начальная ситуация в игре
AI = AI_shot(battle_grownd_player, player_ships)
tipo_AI = AI_shot(battle_grownd_AI, AI_ships)
print(GAME_START)
while True:
    if player == "gamer":
        sum = 0
        for i in AI_ships:
            sum += i.__dict__["health"]
        if way == "d":
            tipo_AI.shotHandler(battle_grownd_AI, AI_ships)
        else:
            game.player_shot(battle_grownd_AI, AI_ships)
        sum2 = 0
        for i in AI_ships:
            sum2 += i.__dict__["health"]
        if sum == sum2:
            print("Промах(")
            player = "AI"
        else:
            print("Попадание)")
            player = "gamer"
        print("поле противника\n----------------------------------------")
        game.printer(battle_grownd_AI, False)
        print("----------------------------------------")


    elif player == "AI":
        sum = 0
        for i in player_ships:
            sum += i.__dict__["health"]
        AI.shotHandler(battle_grownd_player, player_ships)
        sum2 = 0
        for i in player_ships:
            sum2 += i.__dict__["health"]
        if sum == sum2:
            print("компьютер промахнулся")
            player = "gamer"
        else:
            print("компьютер попал")
            game.printer(battle_grownd_player, True)
            player="AI"

    if sum2 == 0:
        break

print(player, "win")
if player == "AI":
    print("winer")
    game.printer(battle_grownd_AI,True)
    print("loser")
    game.printer(battle_grownd_player,True)
if player == "gamer":
    print("winer")
    game.printer(battle_grownd_player,True)
    print("loser")
    game.printer(battle_grownd_AI, True)
if way == "d":
    print("--- %s seconds ---" % (time.time() - start_time))

# 0 - пустое поле
# 1 - поле с кораблем
# 2 - охранные границы
# 3 - попадание
# 4 - мертвый корабль
# 5 - промах
# 6 - видимая охранная граница
