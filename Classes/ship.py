"""объект корабль, все выполнено в парадигмее ООП корабль облатает набором атрибутов и методов, что делает работу очень удобной"""
from copy import deepcopy


class Ship():

    def __init__(self, battle_grownd, xcord, ycord, size, direction):  # работает
        self.cordinates = []  # при инициализации корабля у него появляется набор атрибутов
        self.direction = direction
        self.list_of_cord(ycord - 1, xcord - 1, size, self.direction)  # задаются координаты коробля вычитание из-за
        if self.check_position(battle_grownd):
            self.alivecord = deepcopy(self.cordinates)
            self.health = size  # здоровье равно размеру
            self.state = "alive"  # если здоровье == 0, меняется на dead
            self.frame_maker(battle_grownd, self.state)  # создает вокруг только что созланного коробля охранное поле
        else:
            self.state = "dead"

    def frame_maker(self, battle_grownd, state):
        stateKey = 0  # генерация полей для живых и мертвых кораблей
        stateKeyShip = 0
        if state == "alive":  # не забыть заменить на 0 чтобы сделать невидимым
            stateKey = 2
            stateKeyShip = 1
            cordinates = self.cordinates
        if state == "dead":  # это надо чтобы показать мертвый корабль
            stateKey = 6
            stateKeyShip = 4
            cordinates = self.cordinates
        for i in self.cordinates:
            xcord, ycord = i[0], i[1]
            battle_grownd[xcord][ycord] = stateKeyShip
            try:
                if battle_grownd[xcord - 1][
                    ycord] != stateKeyShip and xcord - 1 > -1:  # пытается расставить вокруг корабля охранное поле
                    battle_grownd[xcord - 1][ycord] = stateKey
            except IndexError:
                pass
            try:
                if battle_grownd[xcord + 1][ycord] != stateKeyShip and xcord + 1 < 11:
                    battle_grownd[xcord + 1][ycord] = stateKey
            except IndexError:
                pass
            try:
                if battle_grownd[xcord][ycord + 1] != stateKeyShip and ycord + 1 < 11:
                    battle_grownd[xcord][ycord + 1] = stateKey
            except IndexError:
                pass
            try:
                if battle_grownd[xcord][ycord - 1] != stateKeyShip and ycord - 1 > -1:
                    battle_grownd[xcord][ycord - 1] = stateKey
            except IndexError:
                pass
            try:
                if battle_grownd[xcord + 1][ycord + 1] != stateKeyShip and xcord + 1 < 11 and ycord + 1 < 11:
                    battle_grownd[xcord + 1][ycord + 1] = stateKey
            except IndexError:
                pass
            try:
                if battle_grownd[xcord - 1][ycord - 1] != stateKeyShip and xcord - 1 > -1 and ycord - 1 > -1:
                    battle_grownd[xcord - 1][ycord - 1] = stateKey
            except IndexError:
                pass
            try:
                if battle_grownd[xcord + 1][ycord - 1] != stateKeyShip and xcord + 1 < 11 and ycord - 1 > -1:
                    battle_grownd[xcord + 1][ycord - 1] = stateKey
            except IndexError:
                pass
            try:
                if battle_grownd[xcord - 1][ycord + 1] != stateKeyShip and xcord - 1 > -1 and ycord + 1 < 11:
                    battle_grownd[xcord - 1][ycord + 1] = stateKey
            except IndexError:
                pass
        return battle_grownd

    def check_position(self, battle_gromnd):
        for i in self.cordinates:
            if i[0] < 0 or i[0] > 9 or i[1] < 0 or i[1] > 9:
                return False
            else:
                if battle_gromnd[i[0]][i[1]] == 1 or battle_gromnd[i[0]][i[1]] == 2:
                    return False
        return True

    def list_of_cord(self, xcord, ycord, size, direction):  # x-ряд y-столбец
        if direction == "v":  # заполняет self координатами в зависимости от положения
            self.cordinates = []
            temp = [xcord, ycord]
            self.cordinates.append(temp)
            for i in range(size - 1):
                xcord += 1
                temp = [xcord, ycord]
                self.cordinates.append(temp)

        if direction == "g":
            self.cordinates = []
            temp = [xcord, ycord]
            self.cordinates.append(temp)
            for i in range(size - 1):
                ycord += 1
                temp = [xcord, ycord]
                self.cordinates.append(temp)

    def get_a_shot(self, xcord, ycord, battle_grownd):
        temp = []
        temp.append(xcord)
        temp.append(ycord)
        if temp in self.alivecord:
            self.health -= 1
            battle_grownd[xcord][ycord] = 3
            self.alivecord.remove(temp)
            self.hit_framemaker(temp, battle_grownd)  # TO-DO
            if self.health == 0:
                self.state = "dead"
                self.frame_maker(battle_grownd, self.state)
        return battle_grownd

    def hit_framemaker(self, cords, battle_grownd):  # TO-DO
        pass

# 0 - пустое поле
# 1 - поле с кораблем
# 2 - охранные границы
# 3 - попадание
# 4 - мертвый корабль
# 5 - промах
# 6 - видимая охранная граница
