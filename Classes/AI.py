from Classes.ship import Ship
from Classes.Battle import Battle
from random import randint


class AI_shot():

    def __init__(self, battle_grownd, ships):
        self.state = "first"
        self.direction = None
        self.cords = None

    def shotHandler(self, battle_grownd, ships):
        if self.state == "first":
            self.FirstShot(battle_grownd, ships)

        elif self.state == "second":
            self.SecondShot(battle_grownd, ships)

        elif self.state == "other":
            self.OtherShot(battle_grownd, ships)
        return battle_grownd, ships

    def cordschecker(self, list_of_cords, battle_grownd):
        to_remuve = []
        for i in list_of_cords:
            if i[0] < 0 or i[0] > 9 or i[1] < 0 or i[1] > 9:
                to_remuve.append(i)
            elif battle_grownd[i[0]][i[1]] == 3 or battle_grownd[i[0]][i[1]] == 4 or battle_grownd[i[0]][i[1]] == 5 or \
                    battle_grownd[i[0]][i[1]] == 6:
                to_remuve.append(i)
        for i in to_remuve:
            list_of_cords.remove(i)
        return list_of_cords

    def FirstShot(self, battle_grownd, ships):  # возможно стоит переработаь систему создав лист доступных координат
        while True:
            x = randint(0, 9)
            y = randint(0, 9)
            if battle_grownd[x][y] == 0:
                battle_grownd[x][y] = 5
                return battle_grownd, ships

            if battle_grownd[x][y] == 2:
                battle_grownd[x][y] = 5
                return battle_grownd, ships

            if battle_grownd[x][y] == 1:
                for i in ships:
                    i.get_a_shot(x, y, battle_grownd)  # возможна ошибка координат
                    if battle_grownd[x][y] == 3 or battle_grownd[x][y] == 4:
                        if i.__dict__["state"] == "dead":
                            self.state = "first"
                            self.cords = x, y
                            return battle_grownd, ships
                        if i.__dict__["state"] == "alive" and self.state == "second":
                            self.state = "other"
                            self.cords = x, y
                        if i.__dict__["state"] == "alive" and self.state == "first":
                            self.state = "second"
                            self.cords = x, y
                            return battle_grownd, ships

    def SecondShot(self, battle_grownd, ships):
        x = self.cords[0]
        y = self.cords[1]
        list_of_cords = [[x + 1, y], [x - 1, y], [x, y + 1], [x, y - 1]]
        self.cordschecker(list_of_cords, battle_grownd)
        shot = randint(0, len(list_of_cords) - 1)

        x = list_of_cords[shot][0]
        y = list_of_cords[shot][1]

        if battle_grownd[x][y] == 0:
            battle_grownd[x][y] = 5
            return battle_grownd, ships

        if battle_grownd[x][y] == 2:
            battle_grownd[x][y] = 5
            return battle_grownd, ships

        if battle_grownd[x][y] == 1:
            for i in ships:
                i.get_a_shot(x, y, battle_grownd)  # возможна ошибка координат
                if battle_grownd[x][y] == 3 or battle_grownd[x][y] == 4:
                    if i.__dict__["state"] == "dead":
                        self.state = "first"
                        self.cords = None
                        return battle_grownd, ships

                    if i.__dict__["state"] == "alive":
                        self.direction = i.__dict__["direction"]
                        self.state = "other"
                        self.cords = [x, y]
                        return battle_grownd, ships

        return battle_grownd, ships

    def OtherShot(self, battle_grownd, ships):
        x = self.cords[0]
        y = self.cords[1]
        if self.direction == "v":
            x1 = x
            while True:
                try:
                    way = randint(0, 1)
                    if way == 0:  # проход вверх
                        if battle_grownd[x1 - 1][y] == 1:
                            for i in ships:
                                i.get_a_shot(x1 - 1, y, battle_grownd)

                                if battle_grownd[x - 1][y] == 3 or battle_grownd[x - 1][y] == 4:
                                    if i.__dict__["state"] == "dead":
                                        self.state = "first"
                                        self.cords = None
                                        return battle_grownd, ships

                                    if i.__dict__["state"] == "alive":
                                        self.direction = i.__dict__["direction"]
                                        self.state = "other"
                                        return battle_grownd, ships

                            return battle_grownd, ships
                        if battle_grownd[x1 - 1][y] == 0:
                            battle_grownd[x1 - 1][y] == 5
                            return battle_grownd, ships
                        if battle_grownd[x1 - 1][y] == 2:
                            battle_grownd[x1 - 1][y] == 5
                            return battle_grownd, ships
                        if battle_grownd[x1 - 1][y] == 6:
                            return battle_grownd, ships
                        if battle_grownd[x1 - 1][y] == 4:
                            return battle_grownd, ships
                        if battle_grownd[x1 - 1][y] == 3:
                            x1 -= 1

                    if way == 1:  # проход вниз
                        if battle_grownd[x1 + 1][y] == 1:
                            for i in ships:
                                i.get_a_shot(x1 + 1, y, battle_grownd)

                                if battle_grownd[x1 + 1][y] == 3 or battle_grownd[x1 + 1][y] == 4:
                                    if i.__dict__["state"] == "dead":
                                        self.state = "first"
                                        self.cords = None
                                        return battle_grownd, ships

                                    if i.__dict__["state"] == "alive":
                                        self.direction = i.__dict__["direction"]
                                        self.state = "other"
                                        return battle_grownd, ships

                        if battle_grownd[x + 1][y] == 0:
                            battle_grownd[x + 1][y] == 5
                            return battle_grownd, ships
                        if battle_grownd[x + 1][y] == 2:
                            battle_grownd[x + 1][y] == 5
                            return battle_grownd, ships
                        if battle_grownd[x + 1][y] == 6:
                            return battle_grownd, ships
                        if battle_grownd[x + 1][y] == 4:
                            return battle_grownd, ships
                        if battle_grownd[x1 + 1][y] == 3:
                            x1 += 1
                except IndexError:
                    pass

        if self.direction == "g":
            y1 = y
            while True:
                try:
                    way = randint(0, 1)
                    if way == 0:  # проход вверх
                        if battle_grownd[x][y1 - 1] == 1:
                            for i in ships:
                                i.get_a_shot(x, y1 - 1, battle_grownd)

                                if battle_grownd[x][y1 - 1] == 3 or battle_grownd[x][y1 - 1] == 4:
                                    if i.__dict__["state"] == "dead":
                                        self.state = "first"
                                        self.cords = None
                                        return battle_grownd, ships

                                    if i.__dict__["state"] == "alive":
                                        self.direction = i.__dict__["direction"]
                                        self.state = "other"
                                        return battle_grownd, ships

                            return battle_grownd, ships
                        if battle_grownd[x][y1 - 1] == 0:
                            battle_grownd[x][y1 - 1] == 5
                            return battle_grownd, ships
                        if battle_grownd[x][y1 - 1] == 2:
                            battle_grownd[x][y1 - 1] == 5
                            return battle_grownd, ships
                        if battle_grownd[x][y1 - 1] == 6:
                            return battle_grownd, ships
                        if battle_grownd[x][y1 - 1] == 4:
                            return battle_grownd, ships
                        if battle_grownd[x][y1 - 1] == 3:
                            y1 -= 1

                    if way == 1:  # проход вниз
                        if battle_grownd[x][y1 + 1] == 1:
                            for i in ships:
                                i.get_a_shot(x, y1 + 1, battle_grownd)

                                if battle_grownd[x][y1 + 1] == 3 or battle_grownd[x][y1 + 1] == 4:
                                    if i.__dict__["state"] == "dead":
                                        self.state = "first"
                                        self.cords = None
                                        return battle_grownd, ships

                                    if i.__dict__["state"] == "alive":
                                        self.direction = i.__dict__["direction"]
                                        self.state = "other"
                                        return battle_grownd, ships
                                if i.__dict__["state"] == "dead":
                                    self.state = "first"
                                if i.__dict__["state"] == "alive":
                                    self.state = "other"
                            return battle_grownd, ships
                        if battle_grownd[x][y1 + 1] == 0:
                            battle_grownd[x][y1 + 1] == 5
                            return battle_grownd, ships
                        if battle_grownd[x][y1 + 1] == 2:
                            battle_grownd[x][y1 + 1] == 5
                            return battle_grownd, ships
                        if battle_grownd[x][y1 + 1] == 6:
                            return battle_grownd, ships
                        if battle_grownd[x][y1 + 1] == 4:
                            return battle_grownd, ships
                        if battle_grownd[x][y1 + 1] == 3:
                            y1 += 1
                except IndexError:
                    pass
# 0 - пустое поле
# 1 - поле с кораблем
# 2 - охранные границы
# 3 - попадание
# 4 - мертвый корабль
# 5 - промах
# 6 - видимая охранная граница
