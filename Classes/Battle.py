"""класс игры, отвечает за сам процесс игры"""

from random import randint
from Classes.ship import Ship


class Battle():

    def fild_creater(self, battle_ground, ships, player):
        ships_footprint = [1, 1, 1, 1, 2, 2, 2, 3, 3, 4]
        i = len(ships_footprint) - 1
        while i >= 0:
            if player == "AI":
                x = randint(1, 10)
                y = randint(1, 10)
                size = ships_footprint[i]
                direction = randint(0, 1)
                if direction == 0:
                    direction = "v"
                if direction == 1:
                    direction = "g"

            if player == "gamer":
                print("enter", ships_footprint[i], "ship head")
                while True:
                    try:
                        x, y = input().split()
                        x = int(x)
                        y = int(y)
                        break
                    except Exception as e:
                        print("введите корректные координаты")
                if ships_footprint[i] != 1:
                    print("v - vertical, g - gorisontal")
                    while True:
                        direction = input()
                        if direction == "v" or direction == "g":
                            break
                        else:
                            print("введите коректное направление")
                if ships_footprint[i] == 1:
                    direction = "g"

            size = ships_footprint[i]
            try:
                ship = Ship(battle_ground, x, y, size, direction)
                if ship.__dict__["state"] == "alive":
                    i -= 1
                    ships.append(ship)
            except Exception as e:
                pass
            if player == "gamer":
                self.printer(battle_ground, True)
        return battle_ground, ships

    def printer(self, battle_grownd, visiable):
        print(" ", "1", "2", "3", "4", "5", "6", "7", "8", "9", "0")
        empty = "."  # 0 2 - empty
        ship = '.'
        if visiable == True:
            ship = "o"  # 1
        guardframe = "*"  # 6
        hit = "@"  # 3
        dead = "x"  # 4
        miss = "-"  # 5
        sum = 0
        for i in range(10):
            if i < 9:
                print(str(i + 1), end=" ")
            if i == 9:
                print("0", end=" ")
            for j in range(10):
                if battle_grownd[i][j] == 0:
                    print(empty, end="")
                    sum += 1
                if battle_grownd[i][j] == 2:
                    print(empty, end="")
                if battle_grownd[i][j] == 3:
                    print(hit, end="")
                if battle_grownd[i][j] == 1:
                    print(ship, end="")
                if battle_grownd[i][j] == 4:
                    print(dead, end="")
                if battle_grownd[i][j] == 5:
                    print(miss, end="")
                if battle_grownd[i][j] == 6:
                    print(guardframe, end="")
                print("", end=" ")
            print("")


    def player_shot(self, AI_battle_grownd, AI_ships):
        print("Огонь!")
        while True:
            while True:
                try:
                    y, x = input().split()
                    x = int(x) - 1
                    y = int(y) - 1
                    AI_battle_grownd[x][y] == 1# проверка на правильность ввода
                    break
                except Exception as e:
                    print("введите корректные координаты")

            if AI_battle_grownd[x][y] == 0:
                AI_battle_grownd[x][y] = 5
                break

            if AI_battle_grownd[x][y] == 1:
                AI_battle_grownd[x][y] = 5
                for i in AI_ships:
                    i.get_a_shot(x, y, AI_battle_grownd)
                return AI_battle_grownd, AI_ships

            if AI_battle_grownd[x][y] == 2:
                AI_battle_grownd[x][y] = 5
                break

            if AI_battle_grownd[x][y] == 3:
                print("неправильное место")

            if AI_battle_grownd[x][y] == 4:
                print("неправильное место")

            if AI_battle_grownd[x][y] == 5:
                print("неправильное место")

            if AI_battle_grownd[x][y] == 6:
                print("неправильное место")

        return AI_battle_grownd, AI_ships

# 0 - пустое поле
# 1 - поле с кораблем
# 2 - охранные границы
# 3 - попадание
# 4 - мертвый корабль
# 5 - промах
# 6 - видимая граница
